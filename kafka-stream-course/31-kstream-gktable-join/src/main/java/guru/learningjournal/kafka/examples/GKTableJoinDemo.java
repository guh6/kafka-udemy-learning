package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.AdClick;
import guru.learningjournal.kafka.examples.types.AdInventories;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Problem:
 *  - Compute the advert clicks by News type
 *
 * Sample Inventory List:
 *  1001:{"InventoryID": "1001", "NewsType": "Sports"}
 *  1002:{"InventoryID": "1002", "NewsType": "Politics"}
 *  1003:{"InventoryID": "1003", "NewsType": "LocalNews"}
 *  1004:{"InventoryID": "1004", "NewsType": "WorldNews"}
 *  1005:{"InventoryID": "1005", "NewsType": "Health"}
 *  1006:{"InventoryID": "1006", "NewsType": "Lifestyle"}
 *  1007:{"InventoryID": "1007", "NewsType": "Literature"}
 *  1008:{"InventoryID": "1008", "NewsType": "Education"}
 *  1009:{"InventoryID": "1009", "NewsType": "Social"}
 *  1010:{"InventoryID": "1010", "NewsType": "Business"}
 *
 * Sample Click Events:
 *  1001:{"InventoryID": "1001"}
 *  1002:{"InventoryID": "1002"}
 *  1003:{"InventoryID": "1003"}
 *  1001:{"InventoryID": "1001"}
 *  1003:{"InventoryID": "1003"}
 *  1005:{"InventoryID": "1005"}
 *  1002:{"InventoryID": "1002"}
 *  1001:{"InventoryID": "1001"}
 *  1003:{"InventoryID": "1003"}
 *  1003:{"InventoryID": "1003"}
 */
public class GKTableJoinDemo {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreName);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        GlobalKTable<String, AdInventories> GKT0 = streamsBuilder.globalTable(AppConfigs.inventoryTopic,
            Consumed.with(AppSerdes.String(), AppSerdes.AdInventories())
        );

        KStream<String, AdClick> KS1 = streamsBuilder.stream(AppConfigs.clicksTopic,
            Consumed.with(AppSerdes.String(), AppSerdes.AdClick())
        );

        // For GlobalKTable, it takes the input keys and can output a new key.
        // The event is triggered IFF the join is successful.
        // (v1, v2) -> v2: because if the match is found, then we want to use the v2 from GKT0 to perform the groupBy
        // using NewsType as the key to get the count.
        KS1.join(GKT0, (k,v) -> k, (v1, v2) -> v2)
                .groupBy((k,v)->v.getNewsType(), Grouped.with(AppSerdes.String(), AppSerdes.AdInventories()))
                .count()
                .toStream().print(Printed.toSysOut());

        logger.info("Starting Stream...");
        KafkaStreams streams = new KafkaStreams(streamsBuilder.build(), props);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Stopping Streams...");
            streams.close();
        }));

    }
}
