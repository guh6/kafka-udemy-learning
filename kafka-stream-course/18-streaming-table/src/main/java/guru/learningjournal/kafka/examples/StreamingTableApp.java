package guru.learningjournal.kafka.examples;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Printed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Requirements:
 *  1. Read the topic as KTable
 *  2. Filter out all other symbols except HDFCBANK and TCS
 *  3. Store the filtered messages in a separate KTable
 *  4. Show the contents of the Final KTable
 *
 *  Use the Producer to send the expected messages in the format: KEY:DOUBLE
 */
public class StreamingTableApp {
    private static final Logger logger = LogManager.getLogger();
    public static void main(final String[] args) {
        final Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        // This tells where the state store we'll be using is
        properties.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreLocation);
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        KTable<String, String> KT0 = streamsBuilder.table(AppConfigs.topicName);
        KT0.toStream().print(Printed.<String,String>toSysOut().withLabel("KT0"));

        KTable<String, String> KT1 = KT0.filter((k,v)-> k.matches(AppConfigs.regExSymbol) && !v.isEmpty(),
                Materialized.as(AppConfigs.stateStoreName)); // This extra argument will put it in this store name
        KT1.toStream().print(Printed.<String,String>toSysOut().withLabel("KT1"));

        KafkaStreams streams = new KafkaStreams(streamsBuilder.build(), properties);

        // Query Server - think of it as adding a REST API to fetch the contents
        QueryServer queryServer = new QueryServer(streams, AppConfigs.queryServerHost, AppConfigs.queryServerPort);
        streams.setStateListener((newState, oldState) -> {
            logger.info("State changing to {} from {}", oldState, newState);
            queryServer.setActive(newState == KafkaStreams.State.RUNNING && oldState == KafkaStreams.State.REBALANCING);
        });

        streams.start();
        queryServer.start();

        Runtime.getRuntime().addShutdownHook(new Thread(()-> {
            logger.info("Shutting down stream.");
            queryServer.stop();
            streams.close();
        }));


    }
}
