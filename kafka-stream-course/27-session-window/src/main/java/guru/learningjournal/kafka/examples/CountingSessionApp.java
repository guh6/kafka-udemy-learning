package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.UserClicks;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Properties;

/**
 * Goal: Count the number of clicks per user session
 *
 * Requirements:
 *  - New session starts with the first click and ends if a user is idle for five minutes
 *
 * Example messages:
 * USR101:{"UserID": "USR101","CreatedTime": "2019-02-05T10:01:00.00Z","CurrentLink": "NULL","NextLink": "Home"}
 * USR102:{"UserID": "USR102","CreatedTime": "2019-02-05T10:02:00.00Z","CurrentLink": "NULL","NextLink": "Home"}
 * USR101:{"UserID": "USR101","CreatedTime": "2019-02-05T10:04:00.00Z","CurrentLink": "Home","NextLink": "Books"}
 * USR101:{"UserID": "USR101","CreatedTime": "2019-02-05T10:10:00.00Z","CurrentLink": "Books","NextLink": "Kafka"}
 * USR102:{"UserID": "USR102","CreatedTime": "2019-02-05T10:10:00.00Z","CurrentLink": "Home","NextLink": "Courses"}
 *
 * USR101:{"UserID": "USR101","CreatedTime": "2019-02-05T10:07:00.00Z","CurrentLink": "Kafka","NextLink": "Preview"}
 * USR101:{"UserID": "USR101","CreatedTime": "2019-02-05T10:19:00.00Z","CurrentLink": "Preview","NextLink": "Buy"}
 */
public class CountingSessionApp {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreName);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        KStream<String, UserClicks> KS0 = streamsBuilder.stream(AppConfigs.topicName,
            Consumed.with(AppSerdes.String(), AppSerdes.UserClicks())
                .withTimestampExtractor(new AppTimestampExtractor())
        );

        KTable<Windowed<String>, Long> KT0 = KS0.groupByKey(Grouped.with(AppSerdes.String(), AppSerdes.UserClicks()))
                .windowedBy(SessionWindows.with(Duration.ofMinutes(5))) // This is NEW
                .count();

        KT0.toStream().foreach((wKey, value) -> logger.info(
                "User ID: " + wKey.key() + " | Window ID: " + wKey.window().hashCode() +
                        " | Window start: " + Instant.ofEpochMilli(wKey.window().start()).atOffset(ZoneOffset.UTC) +
                        " | Window end: " + Instant.ofEpochMilli(wKey.window().end()).atOffset(ZoneOffset.UTC) +
                        " | Count: " + value
        ));

        logger.info("Starting Stream...");
        KafkaStreams streams = new KafkaStreams(streamsBuilder.build(), props);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Stopping Streams...");
            streams.close();
        }));

    }
}
