package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.DepartmentAggregate;
import guru.learningjournal.kafka.examples.types.Employee;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;
/**
 * Given:
 *  - A Kafka topic as employees with the following messages:
 *      101:{"id":"101", "name": "Prashant", "department": "engineering", "salary": 5000}
 *      102:{"id":"102", "name": "John",     "department": "accounts",    "salary": 8000}
 *      103:{"id":"103", "name": "Abdul",    "department": "engineering", "salary": 3000}
 *      104:{"id":"104", "name": "Melinda",  "department": "support",     "salary": 7000}
 *      105:{"id":"105", "name": "Jimmy",    "department": "support",     "salary": 6000}
 *
 * Requirements:
 *  - Create a kafka Streams app to compute the average salary for each department
 *  - Result should have: engineering-4000, accounts-8000, support-6500
 *  - Handle department swaps
 */
public class KTableAggDemo {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreLocation);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG,100);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        KTable<String, Employee> KT1 = streamsBuilder.table(AppConfigs.topicName,
                Consumed.with(AppSerdes.String(), AppSerdes.Employee()));

        // Here, we are creating key=department with value=employee instead of employee.id -> employee
        KT1.groupBy((k,v) -> KeyValue.pair(v.getDepartment(), v), Grouped.with(AppSerdes.String(), AppSerdes.Employee()))
                .aggregate(
                        // Initializer
                        () -> new DepartmentAggregate()
                        .withEmployeeCount(0)
                        .withTotalSalary(0)
                        .withAvgSalary(0D),
                        // Adder
                        (k,v,aggV) -> new DepartmentAggregate()
                        .withEmployeeCount(aggV.getEmployeeCount() + 1)
                        .withTotalSalary(aggV.getTotalSalary() + v.getSalary())
                        .withAvgSalary((aggV.getTotalSalary() + v.getSalary())/(aggV.getEmployeeCount() +1D)),
                        // Subtractor
                        (k,v,aggV) -> new DepartmentAggregate()
                        .withEmployeeCount(aggV.getEmployeeCount() - 1)
                        .withTotalSalary(aggV.getTotalSalary() - v.getSalary())
                        .withAvgSalary((aggV.getTotalSalary() - v.getSalary())/aggV.getEmployeeCount()-1D),
                        //Serializer
                        Materialized.<String, DepartmentAggregate, KeyValueStore<Bytes, byte[]>>as(AppConfigs.stateStoreName)
                                .withKeySerde(AppSerdes.String())
                                .withValueSerde(AppSerdes.DepartmentAggregate())
                ).toStream().print(Printed.<String, DepartmentAggregate>toSysOut().withLabel("Department Salary Average"));

        KafkaStreams streams = new KafkaStreams(streamsBuilder.build(), props);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Stopping Streams");
            streams.close();
        }));
    }
}
