package guru.learningjournal.kafka.examples.serde;

import guru.learningjournal.kafka.examples.types.PosInvoice;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Class where all of the Serdes are defined.
 * The Kafka convention is Serdes.String() so that format should be followed.
 */
public class AppSerdes extends Serdes {

    /**
     * Use this template to create your own Serdes
     */
    static public final class PosInvoiceSerde extends WrapperSerde<PosInvoice> {
        public PosInvoiceSerde() {
            super(new JsonSerializer<>(), new JsonDeserializer<>());
        }
    }
    static public Serde<PosInvoice> PosInvoice() {
        PosInvoiceSerde serde = new PosInvoiceSerde();

        Map<String, Object> serdeConfigs = new HashMap<>();
        serdeConfigs.put(JsonDeserializer.VALUE_CLASS_NAME_CONFIG, PosInvoice.class);
        serde.configure(serdeConfigs, false);

        return serde;
    }
}
