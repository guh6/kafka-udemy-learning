package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.datagenerator.InvoiceGenerator;
import guru.learningjournal.kafka.examples.types.PosInvoice;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicBoolean;

public class RunnableProducer implements Runnable {
    private static final Logger logger = LogManager.getLogger();
    private KafkaProducer<String, PosInvoice> producer;
    private InvoiceGenerator invoiceGenerator;
    private final String topic;
    private final int sleepTimerMs;
    private AtomicBoolean stopper = new AtomicBoolean(false);

    public RunnableProducer(KafkaProducer<String, PosInvoice> producer,
                            InvoiceGenerator invoiceGenerator, String topic, int sleepTimerMs) {
        this.producer = producer;
        this.invoiceGenerator = invoiceGenerator;
        this.topic = topic;
        this.sleepTimerMs = sleepTimerMs;
    }

    @Override
    public void run() {
        while(true) {
            try {
                PosInvoice posInvoice = invoiceGenerator.getNextInvoice();
                logger.info("Sending message to producer. PosInvoice={}", posInvoice);
                producer.send(new ProducerRecord<>(topic, posInvoice.getStoreID(), posInvoice));
                Thread.sleep(sleepTimerMs);
            } catch (Exception e) {
                logger.error("Error sending message to producer", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void shutDown(){
        logger.info("Shutting down producer thread - {}", Thread.currentThread().getId());
        this.stopper.set(true);
    }

}
