package guru.learningjournal.kafka.examples;


import guru.learningjournal.kafka.examples.datagenerator.InvoiceGenerator;
import guru.learningjournal.kafka.examples.serde.JsonSerializer;
import guru.learningjournal.kafka.examples.types.PosInvoice;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PosSimulator {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Please provide command line arguments: topicName noOfProducers produceSpeed");
            System.exit(-1);
        }

        String topicNameArg = args[0];
        int noOfProducersArg = Integer.parseInt(args[1]);
        int produceSpeedArg = Integer.parseInt(args[2]);

        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "POS-Simulator");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092,localhost:9093");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());


        KafkaProducer<String, PosInvoice> kafkaProducer = new KafkaProducer<>(properties);

        // Creates the Thread Pool using ExecutorService
        ExecutorService executorService = Executors.newFixedThreadPool(noOfProducersArg);
        final List<RunnableProducer> runnableProducerList = new ArrayList<>();
        for(int i = 0; i < noOfProducersArg; i++) {
            RunnableProducer runnableProducer = new RunnableProducer(kafkaProducer,
                    InvoiceGenerator.getInstance(),
                    topicNameArg, produceSpeedArg);
            executorService.submit(runnableProducer);
        }

        // Adds fancy shutdown hook, although doesn't seem to work in Ubuntu
        Runtime.getRuntime().addShutdownHook(new Thread(()-> {
            runnableProducerList.stream().forEach(RunnableProducer::shutDown);
            executorService.shutdown();
            try {
                executorService.awaitTermination(produceSpeedArg * 2, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                kafkaProducer.flush();
                kafkaProducer.close();
            }
        }));
    }

}
