package guru.learningjournal.kafka.examples;


import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.PosInvoice;
import org.apache.kafka.common.record.Record;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Requirements:
 *  POS -> Source Processor -> KSO -> filter() -> KS1-> to() -> KS2 -> Shipment
 *                                 -> filter() -> KS3 -> mapValues() -> KS4 -> to() -> KS5 -> Loyalty
 *                                 -> mapValues() -> KS6 -> flatMapValues() -> KS7 -> to() -> KS8 ->  Hadoop-Sink
 */
public class PosFanoutApp {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String args[]) {
        Properties properties = new Properties();
        // We don't need to define the default deserializer because they will be different depending on the  processor
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);

        StreamsBuilder builder = new StreamsBuilder();
        // First Requirement - The streams can be chained together. It's not chained for demo purposes.
        KStream<String, PosInvoice> KS0 = builder.stream(AppConfigs.posTopicName,
                Consumed.with(AppSerdes.String(), AppSerdes.PosInvoice())); // Define what it needs to deserialize to
        KStream<String, PosInvoice> KS1 = KS0.filter((k,v) -> AppConfigs.DELIVERY_TYPE_HOME_DELIVERY.equals(v.getDeliveryType()));
        // KS2
        KS1.to(AppConfigs.shipmentTopicName, Produced.with(AppSerdes.String(), AppSerdes.PosInvoice()));

        // Second Requirement
        KS0.filter((k,v)-> AppConfigs.CUSTOMER_TYPE_PRIME.equals(v.getCustomerType())) // KS3
                .mapValues(RecordBuilder::getNotification)  // KS4
                .to(AppConfigs.notificationTopic, Produced.with(AppSerdes.String(), AppSerdes.Notification())); // KS5

        // Third Requirement
        KS0.mapValues(RecordBuilder::getMaskedInvoice) // KS6
                .flatMapValues(RecordBuilder::getHadoopRecords) // KS7 1-to-many
                .to(AppConfigs.hadoopTopic, Produced.with(AppSerdes.String(), AppSerdes.HadoopRecord())); // KS8

        KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.start();
        logger.info("Started stream.");

        Runtime.getRuntime().addShutdownHook(new Thread(()-> {
            logger.info("Stopping stream");
            streams.close();
        }));
    }
}
