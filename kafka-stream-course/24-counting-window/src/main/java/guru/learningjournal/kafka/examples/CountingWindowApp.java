package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.SimpleInvoice;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.acl.Group;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Properties;

/**
 * Problem:
 *  - Compute the number of invoices in a five-minute window by each store. Use event time semantics.
 *
 * Requirements:
 *  1. create Kafka topic named as simple-invoice
 *  2. Send in the following invoices:
 *  STR1534:{"InvoiceNumber": 101, "CreatedTime": "2019-02-05T10:00:10.00Z", "StoreID": "STR1534", "TotalAmount": 1920}
 *  STR1535:{"InvoiceNumber": 102, "CreatedTime": "2019-02-05T10:00:40.00Z", "StoreID": "STR1535", "TotalAmount": 1860}
 *  STR1534:{"InvoiceNumber": 103, "CreatedTime": "2019-02-05T10:01:11.00Z", "StoreID": "STR1534", "TotalAmount": 2400}
 *  STR1535:{"InvoiceNumber": 104, "CreatedTime": "2019-02-05T10:02:11.00Z", "StoreID": "STR1535", "TotalAmount": 8936}
 *  STR1535:{"InvoiceNumber": 105, "CreatedTime": "2019-02-05T10:03:15.00Z", "StoreID": "STR1535", "TotalAmount": 6375}
 *  STR1534:{"InvoiceNumber": 106, "CreatedTime": "2019-02-05T10:04:12.00Z", "StoreID": "STR1534", "TotalAmount": 9365}
 *  STR1534:{"InvoiceNumber": 107, "CreatedTime": "2019-02-05T10:07:11.00Z", "StoreID": "STR1534", "TotalAmount": 5276}
 */
public class CountingWindowApp {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        props.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreName);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 0);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        // 1. Since we're using Event Time semantics, we'll need to create a custom Timestamp extractor to use the field
        // in our POJO.
        KStream<String, SimpleInvoice> KS0 = streamsBuilder.stream(AppConfigs.posTopicName,
            Consumed.with(AppSerdes.String(), AppSerdes.SimpleInvoice())
            .withTimestampExtractor(new SimpleInvoiceTimeExtractor())  // This is NEW
        );

        KTable<Windowed<String>, Long> KT0 = KS0.groupByKey(Grouped.with(AppSerdes.String(), AppSerdes.SimpleInvoice()))
                .windowedBy(TimeWindows.of(Duration.ofMinutes(5)).grace(Duration.ofMinutes(2))) // This is NEW
                .count();

        KT0.toStream().foreach((wKey, value) -> logger.info(
                "Store ID: " + wKey.key() + " | Window ID: " + wKey.window().hashCode() +
                        " | Window start: " + Instant.ofEpochMilli(wKey.window().start()).atOffset(ZoneOffset.UTC) +
                        " | Window end: " + Instant.ofEpochMilli(wKey.window().end()).atOffset(ZoneOffset.UTC) +
                        " | Count: " + value
        ));

        KafkaStreams streams = new KafkaStreams(streamsBuilder.build(), props);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Stopping Streams");
            streams.close();
        }));

    }

    public static class SimpleInvoiceTimeExtractor implements TimestampExtractor {

        /**
         * Extract the timestamp from the consumer record and return it
         * @param record
         * @param previousTimestamp
         * @return EPOCH Miliseconds
         */
        @Override
        public long extract(ConsumerRecord<Object, Object> record, long previousTimestamp) {
            SimpleInvoice simpleInvoice = (SimpleInvoice) record.value();
            long epochEventTime = Instant.parse(simpleInvoice.getCreatedTime()).toEpochMilli();
            return epochEventTime > 0 ? epochEventTime : previousTimestamp; // Safe guard against negative time
        }
    }
}
