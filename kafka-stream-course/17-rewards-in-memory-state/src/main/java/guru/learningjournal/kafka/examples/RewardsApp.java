package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.Notification;
import guru.learningjournal.kafka.examples.types.PosInvoice;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

/**
 * Requirements:
 *      POS -> Source Processor -> filter(), Prime Customers -> TransformValues(), Notification Event -> to() -> Loyalty
 *
 *  Requirements when using through() to deal with adding an intermediary topic so that a key goes to the desired partition:
 *      POS -> Source Processor -> filter(), Prime Customers -> through() -> TransformValues(), Notification Event -> to() -> Loyalty
 */
public class RewardsApp {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, PosInvoice> KS0 = builder.stream(AppConfigs.posTopicName,
                Consumed.with(AppSerdes.String(), AppSerdes.PosInvoice()));

        KS0.filter((k,v) -> v.getCustomerType().equals(AppConfigs.CUSTOMER_TYPE_PRIME));

        // Now, apply the stateful processor
        // 1. Create a store - this will be an in memory store with a unique name. Also, since everything needs to
        //  be materialized, we need to make sure that the key and values are properly serialized and deserialized
        StoreBuilder kvStoreBuilder = Stores.keyValueStoreBuilder(Stores.inMemoryKeyValueStore(AppConfigs.REWARDS_STORE_NAME),
                AppSerdes.String(), AppSerdes.PosInvoice());
        builder.addStateStore(kvStoreBuilder);

        // 2. Now, we need to create a the stateful processor
        KS0.through(AppConfigs.REWARDS_TEMP_TOPIC, Produced.with(AppSerdes.String(), AppSerdes.PosInvoice(), new RewardsPartitioner()))
            .transformValues(()-> new RewardsTransformer(), AppConfigs.REWARDS_STORE_NAME)
            .to(AppConfigs.notificationTopic, Produced.with(AppSerdes.String(), AppSerdes.Notification()));

        KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            logger.info("Shutting down stream");
            streams.close();
        }));
    }

    /**
     * Class that demonstrates how you can use a store with transformValues processor
     */
    public static class RewardsTransformer implements ValueTransformer<PosInvoice, Notification> {
        private KeyValueStore<String, Double> stateStore;

        @Override
        /**
         * This is called once for each Kafka message. This also initializes the store
         */
        public void init(ProcessorContext context) {
            this.stateStore = (KeyValueStore<String, Double>) context.getStateStore(AppConfigs.REWARDS_STORE_NAME);
        }

        @Override
        public Notification transform(PosInvoice value) {
            Notification notification = new Notification()
                    .withInvoiceNumber(value.getInvoiceNumber())
                    .withCustomerCardNo(value.getCustomerCardNo())
                    .withTotalAmount(value.getTotalAmount())
                    .withEarnedLoyaltyPoints(value.getTotalAmount() * AppConfigs.LOYALTY_FACTOR)
                    .withTotalLoyaltyPoints(0.0);
            Double accumlateRewards = stateStore.get(notification.getCustomerCardNo());
            Double totalRewards;
            if(accumlateRewards != null) {
                totalRewards = accumlateRewards + notification.getEarnedLoyaltyPoints();
            } else {
                totalRewards = notification.getTotalLoyaltyPoints();
            }
            stateStore.put(notification.getCustomerCardNo(), notification.getEarnedLoyaltyPoints());
            notification.setTotalLoyaltyPoints(totalRewards);

            return notification;
        }

        @Override
        public void close() {

        }
    }

    /**
     * Class that demonstrates custom Partitioner - HOWEVER, this is an expensive process!
     */
    public static class RewardsPartitioner implements StreamPartitioner<String, PosInvoice> {

        @Override
        public Integer partition(String topic, String key, PosInvoice value, int numPartitions) {
            return value.getCustomerCardNo().hashCode() % numPartitions;
        }
    }

}
