package guru.learningjournal.kafka.examples;

import guru.learningjournal.kafka.examples.serde.AppSerdes;
import guru.learningjournal.kafka.examples.types.AdImpression;
import guru.learningjournal.kafka.examples.types.CampaignPerformance;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.jupiter.api.*;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AppTopologyTest {
    private static TopologyTestDriver topologyTestDriver;

    @BeforeAll
    static void setup(){
        // This is copied from the main class
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, AppConfigs.applicationID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfigs.bootstrapServers);
        // make sure to use a different store
        props.put(StreamsConfig.STATE_DIR_CONFIG, AppConfigs.stateStoreLocationUT);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        AppTopology.withBuilder(streamsBuilder);
        Topology topology = streamsBuilder.build();

        topologyTestDriver = new TopologyTestDriver(topology, props);
    }

    @Test
    @Order(1)
    @DisplayName("Test the impression flow from the source topic to the final output")
    void impressionFlowTest(){
        AdImpression adImpression = new AdImpression().withImpressionID("100001").withCampaigner("ABC Ltd");
        // Use this to simulate an event
        ConsumerRecordFactory<String, AdImpression> impressionConsumerRecordFactory = new ConsumerRecordFactory<>(
                AppSerdes.String().serializer(), AppSerdes.AdImpression().serializer()
        );

        // Sends the event
        topologyTestDriver.pipeInput(impressionConsumerRecordFactory.create(
                AppConfigs.impressionTopic,
                "10001",
                adImpression
        ));

        // Get the actual output
        ProducerRecord<String, CampaignPerformance> output = topologyTestDriver.readOutput(AppConfigs.outputTopic,
                AppSerdes.String().deserializer(), AppSerdes.CampaignPerformance().deserializer());

        // Assert here
        assertAll(
                () -> assertEquals("ABC Ltd", output.value().getCampaigner()),
                () -> assertEquals("2", output.value().getAdImpressions().toString())
        );
    }

    @AfterAll
    static void cleanUpAll() {
        topologyTestDriver.close();
    }

}