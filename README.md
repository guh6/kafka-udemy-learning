# Introduction #

This repository contains notes and working examples from various Udemy Learnings on Kafka. This includes:

* https://mmm.udemy.com/course/apache-kafka/


* https://mmm.udemy.com/course/kafka-streams-real-time-stream-processing-master-class/


Directory contents:
```
├── kafka-beginners-course
├── kafka-data
│   ├── config
│   └── data
├── README.md
└── scripts
    ├── envars.sh
    ├── README.md
    ├── start_broker0
    ├── start_broker1
    └── start_zookeeper
```
	
**kafka-beginners-course** contains Java examples from Apache Kafka Series - Learn Apache Kafka for Beginners v2

**kafka-data** contains configuration files for bootstrap zookeeper and 2 brokers

**scripts** contains helper scripts to start the bootstrap servers

---
# Notes #

* Topics: a particular stream of data.
    * Similar to a table in a database without all the constraints.
    * You can have as many topics as you want
    * A topic is identified by its name


* Topics are split in partitions
    * Each partitions is ordered
    * Each message within a partition gets an incremental id, called offset
    * Offset only have a meaning for a specific partition
    * Order is guaranteed only within a partition(not across partitions)
    * Data is kept only for a limited time(default=1wk)
    * Once data is written to a partition, it can't be changed(immutability)
    * Data is assigned randomly to a partitions unless a key is provided
* Example:
    * You have a fleet of trucks, each truck reports its GPS position to Kafka.
    * You can have a topic truck_gps that contains the position of all trucks
    * Each truck will send a message to Kafka every 20s, each message will contain the truck ID and the position(Lat&Long)
    * We choose to create a topic with 10 partitions(arbitrary)

* Partitions & Segments
    * Topics are made of partitions
    * Partitions are made of segments (files)!
    * Only one segment is ACTIVE(the one data is being written to)
    * Two segment settings:
        * log.segment.bytes: the max size of a single segment in bytes
        * log.segment.ms: the time Kafka will wait before committing the segment if not full
    * Segments come with two indexes(files):
        * An offset to position index: allows Kafka where to read to find a message
        * A timestamp to offset index: allows kafka to find messages with a timestamp
        * Therefore, Kafka knows where to find data in a constant time
        * A smaller log.segment.bytes means:
            * more segments per partitions, log compaction happens more often, Kafka has to keep more files opened!
        * A smaller log.segment.ms means:
            * You set a max frequency for log compaction (more frequent triggers)

* Log Cleanup Policies
    * Many Kafka clusters make data expire, according to a policy = log Cleanup
    * Policy 1: log.cleanup.policy=delete (default for all user topics)
        * Based on age of data. default = 1 week
        * Based on max size of log. default =-1 == infinite
    * Policy 2: `log.cleanup.policy=compact` (Kafka default for topic __consumer__offsets)
        * Delete based on keys of your messages
        * Will delete old duplicate keys after the active segment is committed
        * Allows for infinite time and space retention
    * Why & When ?
        * Deleting data allows you to:
            * control the size of the data on the disk, delete obsolete data
            * overall: limit maintenance work on the Kafka Cluster
        * How often does log cleanup happen?
            * happens on your partition segments!
            * smaller / more segments means that log cleanup will happen more often!
            * log cleanup shouldn't happen too often => takes CPU and RAM resources
            * The cleaner checks for work every 15 seconds (`log.cleaner.backoff.ms`)
    * Delete
        * `log.retention.hours`:
            * number of hours to keep data for (default is 168 - 1 week)
            * higher number means more disk space
            * lower number means that less data is retained(if your consumers are down for too long, they can miss data)
        * `log.retention.bytes`:
            * max size in Bytes for each partition (default -1 - infinite)
            * useful to keep the size of a log under a threshold
        * Two common pair of options:
            * one week of retention. `log.retention.hours=168`, `log.retention.bytes=-1`
            * infinite bounded by 500MB: `log.retention.hours=17520`, `log.retention.bytes=524288000`
* Log Cleanup Policy: Compact
    * log compaction ensures that your log contains at least the last known value for a **specific key** within a partition
    * very useful if we just require a SNAPSHOT instead of full history (such as for a data table in a database)
    * the idea is we only keep the latest update for a key in our log
    * Guarantees
        * Any consumer that is reading from the tail of a log(most current data) will still see all the messages sent to the topic
        * Ordering of messages it kept, log compaction only removes some messages but does not re-order them
        * The offset of a message is immutable(it never changes). Offsets are just skipped if a message is missing
        * Deleted records can still be seen by consumers for a period of `delete.retention.ms` (default=24hrs)
    * Myth Busting
        * It doesn't prevent you from pushing duplicate data to Kafka
        * De-duplication is done after a segment is committed
        * Your consumers will still read from tail as soon as the data arrives
        * It doesn't prevent you from reading duplicate data from Kafka
        * Log compaction can fail from time to time
            * It is an optimization and the compaction thread might crash
            * Make sure you assign enough memory to it and that it gets triggered
            * Restart Kafka if log compaction is broken
    * Configured by `log.cleanup.policy=compact`
        * `segment.ms` default = 7 days. Max amount of time to wait to close active segment
        * `segment.bytes` default 1G. Max size of a segment
        * `min.compaction.lag.ms` (default 0) how long to wait before a message can be compacted
        * `delete.retention.ms` (default 24 hrs) wait before deleting data marked for compaction
        * `min.cleanable.dirty.ratio)` default 0.5 higher => less, more efficient cleaning. lower => opposite

* Brokers
    * A Kafka cluster is composed of multiple brokers(servers)
    * Each broker is identified with its ID (integer)
    * Each broker contains certain topic partitions
    * After connecting to nay broker(bootstrap broker), you will be connected to the entire cluster
    * A good number to get started is 3


* Topic replication factor
    * Topics should have a replication factor >1 (usually between 2 and 3)
    * This way if a broker is down, another broker can serve the data
    * At any given time, only ONE broker can be a leader for a given partition
        * Only that leader can receive and serve data for a partition
        * The other brokers will synchronize the data
        * Each partition has one leader and multiple ISR (in-sync replica)


* Producers
    * write data to topics (which is made of partitions)
    * automatically know which broker and partition to write to
    * in case of broker failures, producers will automatically recover
    * can choose to receive acknowledge of data writes:
        * acks=0: producer won't wait for ack (possible data loss)
        * acks=1: producer will wait for leader acknowledgment (limited data loss)
        * acks=all: leader + replicas acknowledgment(no data loss)
    * Producers can choose to send a key with the message(string, number,etc.)
        * if key=null, data is sent round robin
        * if key is sent, then all messages for that key will always go to the same partition
        * A key is basically sent if you need message ordering for a specific field(ex: truck_id)


* Consumers
    * Read data from a topic identified by name
    * Know which broker to read from
    * In case of broker failures, consumers know how to recover
    * Data is read in order within each partitions


* Consumer Groups
    * Consumers read data in consumer Groups
    * Each consumer within a group reads from exclusive partitions
    * If you have more consumers than partitions, some consumers will be inactive
    * Which is way it is important to have the number of partitions = consumers


* Consumer Offsets
    * Kafka stores the offsets at which a consumer group has been reading
    * The offsets committed live in a Kafka topic named __consumer__offsets
    * When a consumer in a group has processed data received from Kafka, it should be committing the Offsets
    * If a consumer dies, it will be able to read back from where it left off thanks to the committed consumer offsets.
* Delivery semantics for consumers
    * Consumers choose when to commit offsets
    * 3 delivery semantics:
        * At most once: committed as soon as the message is received. Message will be lost if processing goes wrong.
        * At least once (usually preferred): committed only after the message is processed. Can result in duplicate processing so the processing must be idempotent.
        * Exactly once: Can be achieved for Kafka to Kafka workflows using Kafka Streams API. For Kafka to External System workflows, use an idempotent consumer.

* Consumer Pool Behavior
    * Kafka consumers have a "poll" model, while many other messaging bus in enterprises have a "push" model.
    * This allows consumers to control where in the log they want to consume, how fast, and gives them the ability to replay events
    * Broker return data immediately if possible. Otherwise empty after timeout.
    * `Fetch.min.bytes` (default 1):
        * Controls how much data you want to pull at least on each request
	* Helps improving throughput and decreasing request number at the cost of latency
    * `Max.poll.records` (default 500):
	* Controls how many records to receive per poll request
	* Increase if your messages are very small and have a lot of available RAM
	* Good to monitor how many records are polled per request
    * `Max.partitions.fetch.bytes` (default 1MB):
	* max data returned by the broker per partition
	* If you read from 100 partitions, you'll need a lot of RAM
    * `Fetch.max.bytes` (default 50MB):
	* max dat areturned fro each fetch request. Covers multiple partitions
	* The consumer performs multiple fetches in parallel
    * Change the settings only if your consumer maxes out on throughput already

* Consumer Offset Commits Strategies (2 most common)
    * (easy) enable.auto.commit=true & synchronous processing of batches
    	* With auto-commit, offsets will be committed automatically for you at regular interval (auto.commit.interval.ms=5000 by default) everytime you call .poll()
   	* If you don't use synchronous processing, you will be in "at-most-once" behvaior because offsets will be committed before your data is processed

```
while(true){
  List<Records> batch = consumer.poll(Duration.ofMillis(100));
  doSomethingSynchronous(batch)
}
```
    * (medium) enable.auto.commit=false & syncrhonous processing of batches
	* You control when you commit offsets and what's the condition for committing them.
        * Example: accumulating records into a buffer and the nflushing the buffer to a database + committing offsets then.
```
while(true) {
  batch += consumer.poll(Duartion.ofMillis(100))
  if isReady(batch) {
    doSomethingSynchronous(batch)
    consumer.commitSync();
  }
}
```


* Consumer Offset Reset Behavior
    * A consumer is expected to read from a log continously
    * If your application has a bug, your consumer can be down
    * If Kafka has a retention of 7 days and your consumer is down for more than 7 days, the offsets are invalid.
    * The behavior for the consumer is to use `auto.offset.reset=latest|earliest|none`
	* latest will read from end of log
	* earliest will read from start of log
	* none will throw exception if no offset is found
    * Consumer offsets can be lost if:
	* consumer hasn't read new data in 1 day (Kafka <= 2.0)
	* consumer hasn't read new data in 7 days (Kafka >= 2.0)

* Replaying data for Consumers
    * To replay data for a consumer group:
	* Take all the consumers from a specific group down
	* use `kafka-consumer-groups` command to set offset to what you want
	* Restart consumers

* Controlling Consumer Liveliness
    * Consumers in a Group talk to a Consumer Groups Coordinator
    * To detect consumers that are "down", there is a "heartbeat" mechanism and a "poll" mechanism
    * To avoid issues, consumers are encouraged to process data fast and poll often

* Consumer Heartbeat Thread
    * `Session.timeout.ms` default=10s
	* Heartbeats are sent periodically to the broker
	* If no heartbeat is sent during that period, the consumer is considered dead
	* Set even lower to faster consumer rebalances
    * `Heartbeat.interval.ms` default=3s
	* How often to send heartbeats
	* Usually set to 1/3rd of `session.timeout.ms`
    * Take-away: This mechanism is used to detect a consumer app being down. Don't change it

* Consumer Poll Thread
    * `max.poll.interval.ms` default=5mins
	* Max amount of time between two .poll() calls before declaring the consumer dead
	* This is particularly relevant for Big Data frameworks like Spark in case the processing takes time
    * Take-away: This mechanism is used to detect a data processing issue with the consumer. Change this if your processing takes a long time or make the processing smaller and more often


* Kafka Broker Discovery
    * Every Kafka broker is also called a bootstrap server
    * You only need to connect to one broker and you will be connected to the entire cluster
    * Each broker knows about all brokers, topics and partitions (metadata)


* Zookeeper
    * Manages the brokers and keeps a list of them
    * Helps in performing leader election for partitions
    * Sends notifications to Kafka in case of changes(e.g., new topic, broker dies, broker comes up, delete topics, etc...)
    * Kafka can't work without Zookeeper
    * Zookeeper by design operates with an odd number of servers
    * Has a leader (handle writes) the rest of the servers are followers(handle reads)
    * Zookeeper does not store consumer offsets with Kafka > v.10


* Kafka Guarantees
    * Messages are appended to a topic-partition in the order they are sent
    * Consumers read messages in the order stored in a topic-partition
    * With a replication factor of N, producers and consumers can tolerate up to N-1 brokers being down
        * E.g., N=3, allows for one broker to be taken down for maintenance and another unexpectedly.
    * As long as the number of partitions remains constant for a topic(no new partitions), the same key will always go to the same partition.

* Producers Acks Deep Dive (Most important setting for Producers)
    * acks = 0 (no acks)
        * No response is requested
        * If the broker goes offline or an exception happens, we won't know and will lose data
        * Useful for data where it's okay to potentially lose messages:
            * Metrics collection
            * Log collection
    * acks = 1 (leader acks, default as of 2.1)
        * Leader response is requested, but replication is not guarantee (happens in background)
        * If an ack is not received, the producer may retry
        * If the leader broker goes offline but replicas haven't replicated the data yet, we have a data loss.
    * acks = all(replicas acks)
        * Leader + Replicas ack requested
        * Added latency and safety (not that much latency)
        * No data loss if enough replicas
        * Necessary setting if you don't want to lose data
            * Must be used in conjunction with `min.insync.replicas` can be set at the broker or topic level(override)
            * min.insync.replicas=2 implies that at least 2 brokers that are ISR(including leader) must respond that they have the data.
            * That means if you use replication.factor=3, min.insync=2, acks=all, you can only tolerate 1 broker going down, otherwise the producer will receive an exception on send (NOT_ENOUGH_REPLICAS exception, up to the Producer to retry).

* Producer Retries
    * In case of transient failures, developers are expected to handle exceptions, otherwise the data will be lost.
    * Example of transient failure:
        * `NotEnoughReplicasException`
    * There is a `retries` setting
        * defaults to 0 for Kafka <=2.0
        * defaults to MAX INT(21474....) for Kafka >= 2.1
    * The `retry.backoff.ms` setting is by default 100ms
        * the producer won't try the request forever, it's bounded by the timeout
    * `delivery.timeout.ms` = 120 000 ms == 2 minutes
        * intuitive Producer Timeout that will retry for up to 2 minutes
        * Records will be failed if they can't be acknowledge in deliver.timeout.ms
        * If you want it to retry forever, update this value
    * In case of retries, there is a chance that messages will be sent out of order(if a batch as failed to be sent).
        * If you rely on key-based ordering, that can be an issue
        * For this, you can set the setting while controls how many produce requests can be made in parallel: `max.in.flight.requests.per.connection`
            * Default: 5
            * Set to 1 if you need to ensure ordering but may impact throughput

* Idempotent Producer
    * Problem: the Producer can introduce duplicate messages in Kafka due to network errors.
    * In Kafka >.11, you can define an idempotent producer which won't introduce duplicates on network errors
    * Great way to guarantee a stable and safe pipeline
    * They come with:
        * retries = Integer.MAX_VALUE
        * `max.in.flight.requests=1`(Kafka ==0.11) or
        * `max.in.flight.requests=5`(Kafka >=1.0 - higher performance * keep ordering)
        * `acks=all`
    * See here for design docs - https://issues.apache.org/jira/browse/KAFKA-5494
    * Just set:
        * `producerProps.put("enable.idempotence", true);`

* Safe producer Summary & Demo
    * Kafka <0.11
        * acks=all(producer level): ensures data is properly replicated before an ack is received
        * min.insync.replicas=2(broker/topic level): Ensures two brokers in ISR at least have the data after an ack
        * retries=MAX_INT(producer level): Ensures transient errors are retried indefinitely
        * max.inflight.requests.per.connection=1(producer level): Ensures only one request is tried at any time, preventing message re-ordering in case of retries
    * Kafka >= 0.11
        * enable.idempotence=true(producer level) + min.insync.replicas=2(broker/topic level)
            * implies acks=all, retries=MAX_INT, max.in.flight.requests.per.connection=1/5
            * keeps ordering guarantees and improving performance!
    * **Running a safe producer might impact throughput and latency, always test for your use case**

* Message Compression
    * Producer usually send data that is text-based, for example with JSON data
    * In this case, it is important to apply compression to the producer.
    * Compression is enabled at the Producer level and doesn't require any configuration change in the Brokers or in the Consumers
    * `compression.type` can be `none`(default), `gzip`, `lz4`, `snappy`
    * Compression is more effective for bigger batches
        * Compressed batch has much smaller producer request size (compression ratio up to 4x!)
        * Faster to transfer data over the network => less latency
        * Better throughput
        * Better disk utilization in Kafka (stored messages on disk are smaller)
    * Disadvantages(very minor):
        * Producers must commit some CPU cycles for compression
        * Consumers must commit some CPU cycles for decompression
    * Overall:
        * Consider testing `snappy` or `lz4` for optimal speed/compression ratio
        * Always use compression in production and especially if you have high throughput
        * Consider tweaking `linger.ms` and `batch.size` to have bigger batches and therefore more compression and higher throughput

* Linger.ms & batch.size
    * By default, Kafka tries to send records as soon as possible
        * It will have up to 5 requests in in flight
        * After this, if more messages have to be sent while others are in flight, Kafka is mart and will start batching them while they wait to send them all at once.
    * This smart batching allows Kafka to increase throughput while maintaining very low latency
    * Batches have higher compression ratio so better efficiency
    * `linger.ms`: Number of ms a producer is willing to wait before sending a batch out. Default=0
        * By introducing some lag, we increase chances of messages being sent together in a batch
        * At the expense of introducing a small delay, we can increase throughput, compression and efficiency of our producer
    * If a batch is full before the end of `linger.ms` period, it will be sent to Kafka right away.
    * `batch.size`: Max number of bytes that will be included in a batch. The default is 16KB.
        * Increasing a batch size to something like 32KB or 64KB can help increasing compression, throughput and efficiency of requests
        * Any message that is bigger than the batch size will not be batched
        * A batch is allocated per partition, so make sure that you don't  set it to a number that's too high, otherwise you'll waste memory!
        * You can monitor the average batch size metric using Kafka Producer Metrics

* Producer Default Partitioner and how keys are hashed
    * By default, keys are hashed using murmur2 algorithm
    * It is possible to override the behavior (partitioner.class) but ideally you should not
    * Formula: `targetParition = Utils.abs(Utils.murmur2(record.key())) % numPartitions;`

* Max.block.ms & buffer.memory
    * If the producer produces faster than the broker can take, the records will be buffered in memory
    * buffer.memory = 33554432(32MB): the size of the send buffer
    * That buffer will fill up over time and fill back down eventually
    * If buffer is full, then .send() method will start to block (won't return right away)
    * max.block.ms=60000: the time the .send() will block until throwing an exception. Exceptions are basically thrown when:
        * The producer has filled up its buffer
        * The broker is not accepting any new data
        * 60 seconds has elapsed.
    * If you hit an exception that usually means your brokers are down or overloaded as they can't respond to requests

* Delivery Semantics
    * **At Most Once** : Offsets are committed as soon as the message batch is received. If the processing goes wrong, the message will be lost and won't be read again.
    * **At Least Once** : Offsets are committed after the message is processed. If the processing goes wrong, the message will be read again. This can result in duplicate processing of messages. Make sure your processing is **idempotent**.
    * **Exactly Once**: This is only for Kafka to Kafka workflows using Streams API.
    * For most applications, you should use at least once processing and ensure your transformations/processing are idempotent.

* Kafka Connect
    * all about code & connectors re-use!
    * Four Common Kafka Use Cases:
	* Source => Kafka: Producer API; Kafka Connect Source
	* Kafka => Kafka: Consumer, Producer API; Kafka Streams
	* Kafka => Sink: Consumer API; Kafka Connect Sink
	* Kafka => App: Consumer AP; Kafka Connect Sink
    * Simplify & improve getting data in and out of Kafka
    * Simplify transforming data within Kafka without relying on external libs
    * It is tough to achieve Fault Tolerance, Idempotence, Distribution, Ordering
    * Source Connectors to get data from Common Data Sources
    * Sink Connectors to publish that data in Common Data Stores
    * Make it easy for non-experienced dev to quickly get their data reliably into Kafka
    * Part of your ETL pipeline
    * Scaling made easy from small pipelines to company-wide pipelines
    * Re-usable code!
* Kafka Streams
    * Java library that allows easy data processing and transformation within Kafka
    * Allows for data transformations, enrichment, fraud detection, monitoring & alerting, etc.
    * No need to create a separate cluster
    * Highly scalable, elastic and fault tolerant
    * One record at a time processing (no batching)
    * Works for any app size

* Schema Registry
    * Utilizing a schema registry has a lot of benefits
    * BUT it implies you need to:
	* set it up well
	* make sure it's highly available
	* partially change the producer and consumer code
    * Apache Avro as a format is awesome but has a learning curve
    * The schema registry is free and open sourced, created by Confluent

* Which Kafka APIs to use? See https://medium.com/@stephane.maarek/the-kafka-api-battle-producer-vs-consumer-vs-kafka-connect-vs-kafka-streams-vs-ksql-ef584274c1e

* Partitions Count, Replication Factor
    * They impact performance and durability of the system overall
    * It is best to get the parameters right the first time!
	* If the Partitions count increases during a topic lifecycle, you will break your keys ordering guarantees
	* If the Replication Factor increases during a topic lifecycle, you put more pressure on your cluster, which can lead to unexpected performance decrease
    * Partitions Count
	* Each partition can handle a throughput of a few MB/s (measure it for your setup!)
	* More partitions implies:
	    * Better parallelism, better throughput
	    * Ability to run more consumers in a group to scale
            * Ability to leverage more brokers if you have a large cluster
	    * BUT more elections to perform for Zookeeper
	    * BUT more files opened on Kafka
	* Guidelines:
	    * Partitions per topic = MILLION DOLLAR QUESTION
  	    * (Intuition) Small cluster (<6 brokers): 2 x # of brokers
	    * (Intuition) Big cluster (>12 brokers): 1 x # of brokers
	    * Adjust for number of consumers you need to run in parallel at peak throughput
	    * Adjust for producer throughput (increase if super-high throughput or projected increase in the next 2 years)
	    * Test! Every Kafka cluster will have different performance.
	    * Don't create a topic with 1000 partitions! Or 2 partitions!
    * Replication Factor
	    * Should be at least 2, usually 3, maximum 4
	    * The higher the replication factor (N):
	 	* Better resilience of your system (N-1 brokers can fail)
		* BUT more replication (higher latency if acks=all)
		* BUT more disk space on your system (50% more if RF is 3 instead of 2)
	    * Guidelines:
		* Set it to 3 to get started (you must have at least 3 brokers for that)
		* If replication performance is an issue, get a better broker instead of less RF
		* Never set it to 1 in production
    * Clusters guidelines
	    * A broker should not hold more than 2k-4k partitions across all topics of that broker
	    * should have a max of 20k partitions across all brokers
	    * in case brokers going down, Zookeeper needs to perform a lot of leader elections
 	    * if you need more partitions in your cluster, add brokers instead
	    * If you need more than 20k partitions, follow the Netflix model and create more Kafka clusters

* unclean.leader.election
    * If all your In Sync Replicas die(but you still have out of sync replicas up), you have the following option:
        * Wait for an ISR to come back online (default)
        * Enable `unclean.leader.election=true` and start producing to non ISR partitions
    * If you enable `unclean.leader.election=true`, you improve availability, but you will lose data because other messages on ISR will be discarded.
    * Overall this is a very dangerous setting and its implication must be understood fully before enabling it.
    * Use caess include: metrics collection, log collection, and other cases where data loss is somewhat acceptable, at the trade-off of availability.


## Architecture & Support ##
* Kafka Cluster Setup
    * You want multiple brokers in different data centers to distribute your load. You also want a cluster of at least 3 zookeeper
        * If AWS, this is 3 AZs
    * Gotchas
        * It's not easy to setup a cluster
        * You want to isolate each Zookeeper & Broker on separate servers
        * Monitoring needs to be implemented
        * Operations have to be mastered
        * You need a really good Kafka Admin
        * An Alternative: many different "Kafka as a Service" offerings on the web. No operational burdens
* Kafka Monitoring and Operations
    * Kafka exposes metrics through JMX
    * Highly important for Monitoring
    * Common places to host:
      * ELK
      * Datadog
      * NewRelic
      * Confluenct Control Centre
      * Promothesus
    * Important metrics:
      * Under replicated partitions: number of partitions are having problems with IRS(in-sync replicas). May indicate a high load on the system
      * Request Handlers: utilization of threads for IO, network, etc... overall utilization of an broker
      * Request timing: how long it takes to reply to requests. Lower is better, as latency will be improved. Look at 99.9 percentile.
    * Ops team must be able to perform the following:
        * Rolling restart of Brokers
        * Updating configurations
        * Rebalancing Partitions
        * Increasing RF
        * Adding a Broker
        * Replacing a Broker
        * Removing a Broker
        * Upgrading a Kafka Cluster with zero downtime


## Kafka Security ##
* Authentication
    * SSL auth: clients authenticate to Kafka using SSL certificates
    * SASL auth:
        * PLAIN: clients auth using username / password (weak - easy to setup)
        * Kerberos: such as LDAP (strong - hard to setup)
        * SCRAM: username / password (strong - medium to setup)
* Authorization
    * ACL have to be maintained by admin and onboard new users

---
# Setting up Kafka

## From Source
1. Download the Kafka code: https://www.apache.org/dyn/closer.cgi?path=/kafka/2.7.0/kafka_2.12-2.7.0.tgz
2. `tar xvf <dir>`
3. Add the bin directory to $PATH

## Configure
4. `mkdir -p data/zookeeper`
5. Set `dataDir=<absolute_path>/data/zookeeper` in config/zookeeper.properties
6. `mkdir -p data/kafka`
7. In `config/server.properties`, update `log.dirs` to absolute path from (6).

## Start Zookeeper
8. Start server(port=2181): `zookeeper-server-start.sh config/zookeeper.properties`

## Start Broker
9. Start server(port=9092) `kafka-server-start.sh config/server.properties`

## From Confluent CLI
1. Download https://docs.confluent.io/5.4.2/cli/installing.html
2. `./confluent local services start` TBD, need to figure out why this doesn't work


## From Docker
1. Follow https://github.com/conduktor/kafka-stack-docker-compose


# CLI
## Topics
**kafka-topics**

Create and List Topics
```
kafka-topics.sh --bootstrap-server localhost:9092 --topic first_topic --create
kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic --create --partitions 3 --replication-factor 1
kafka-topics.sh --zookeeper 127.0.0.1:2181 --list

# Example
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-topics.sh --zookeeper 127.0.0.1:2181  --delete --topic first_topic
Topic first_topic is marked for deletion.
Note: This will have no impact if delete.topic.enable is not set to true.
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic --create --partitions 3 --replication-factor 1
WARNING: Due to limitations in metric names, topics with a period ('.') or underscore ('_') could collide. To avoid issues it is best to use either, but not both.
Created topic first_topic.
```

Describe Topic
```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic --describe
Topic: first_topic	PartitionCount: 3	ReplicationFactor: 1	Configs:
	Topic: first_topic	Partition: 0	Leader: 0	Replicas: 0	Isr: 0
	Topic: first_topic	Partition: 1	Leader: 0	Replicas: 0	Isr: 0
	Topic: first_topic	Partition: 2	Leader: 0	Replicas: 0	Isr: 0
```

* Leader is broker (id=0)

## Producer
**kafka-console-producer**

```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic
>hello Gu
>Learning Kafka!
>just another message :)
>This should be the 4th message
>^C
```

With ack option
```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic --producer-property acks=all
Some message that is acked
>^C
```

**What happens if you send a message to a topic that does not exist?**

```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic_new --producer-property acks=all
>hey this topic does not exist
[2021-01-14 21:07:33,264] WARN [Producer clientId=console-producer] Error while fetching metadata with correlation id 3 : {first_topic_new=LEADER_NOT_AVAILABLE} (org.apache.kafka.clients.NetworkClient)
```

Well, Kafka tries to create the topic and the same message from before succeeds because it's a WARNING

```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic_new --producer-property ackllal
>test this a second time
```

In the Kafka log, we can see:

```
[2021-01-14 21:07:33,298] INFO [Partition first_topic_new-0 broker=0] No checkpointed highwatermark is found for partition first_topic_new-0 (kafka.cluster.Partition)
[2021-01-14 21:07:33,298] INFO [Partition first_topic_new-0 broker=0] Log loaded for partition first_topic_new-0 with initial high watermark 0 (kafka.cluster.Partition)

guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic_new --describe
Topic: first_topic_new	PartitionCount: 1	ReplicationFactor: 1	Configs:
	Topic: first_topic_new	Partition: 0	Leader: 0	Replicas: 0	Isr: 0
```

BUT this is not what we want because the default has a low replication factor. You can change the default in `config/server.properties` under Log Basics.

**Producer with keys:**
```
kafka-console-producer --broker-list 127.0.0.1:9092 --topic first_topic --property parse.key=true --property key.separator=,
```

## Consumer
**kafka-console-consumer**

`kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic`
* This will only read messages that will come from the producer but not any past messages

If you want to read it from the beginning but ordering is not guaranteed:

`kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --from-beginning`

Consumer with keys:

`kafka-console-consumer --bootstrap-server 127.0.0.1:9092 --topic first_topic --from-beginning --property print.key=true --property key.separator=,`

### Consumer Groups
`kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --group my-first-application`
* The messages are now split between application for each partition.

If you do from beginning with group, then the offset will start from 0. If you run the same command again, the offset will be commited and you will start from the new offset.

Example:

```
$ kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic --producer-property acks=all
.
.
.
^CProcessed a total of 27 messages

$ kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --group my-second-application --from-beginning
^CProcessed a total of 0 messages
```

One key difference between consumer groups and non-consumer group is that if the consumer stops reading and resumes later on, while there are messages being produced, then when the consumer resumes reading, it will receive the past messages that it has not read. This is because of the offset.

Kafka automatically creates a group for the console consumer if --group option is not given.

kafka-consumer-groups
```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-consumer-groups.sh --bootstrap-server localhost:9092 --list
my-first-application
my-second-application

```

```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group my-first-application

Consumer group 'my-first-application' has no active members.

GROUP                TOPIC           PARTITION  CURRENT-OFFSET  LOG-END-OFFSET  LAG             CONSUMER-ID     HOST            CLIENT-ID
my-first-application first_topic     0          9               13              4               -               -               -
my-first-application first_topic     1          7               14              7               -               -               -
my-first-application first_topic     2          11              13              2               -               -               -
```
* Lag is the number of unread messages

### Topic Configuration
Create the configured-topic to play with
`kafka-topics.sh --zookeeper 127.0.0.1:2181 --create --topic configured-topic --partitions 3 --replication-factor 1`

**kafka-configs**

`kafka-configs` will show all possible options.
`kafka-configs.sh --zookeeper 127.0.0.1:2181 --entity-type topics --entity-name configured-topic --add-config`
`kafka-configs.sh --zookeeper 127.0.0.1:2181 --entity-type topics --entity-name configured-topic --add-config min.insync.replicas=2 --alter`
Example:
```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-configs.sh --zookeeper 127.0.0.1:2181 --entity-type topics --entity-name configured-topic --add-config min.insync.replicas=2 --alter
Warning: --zookeeper is deprecated and will be removed in a future version of Kafka.
Use --bootstrap-server instead to specify a broker to connect to.
Completed updating config for entity: topic 'configured-topic'.
```

### Resetting Offsets

```
guoyang@Ubuntu:~/Projects/kafka-udemy-learning/kafka_2.12-2.7.0$ kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group my-first-application --reset-offsets --to-earliest --execute --topic first_topic

GROUP                          TOPIC                          PARTITION  NEW-OFFSET     
my-first-application           first_topic                    0          0              
my-first-application           first_topic                    1          0      
```


# Useful Tools
* UI: https://www.conduktor.io/docs
* Alternative CLI: https://github.com/edenhill/kafkacat (https://medium.com/@coderunner/debugging-with-kafkacat-df7851d21968)
---
# Java
## Configs
* https://kafka.apache.org/documentation/#producerconfigs
* https://kafka.apache.org/documentation/#consumerconfigs

## Client Bi-Directional Compatibility
* Older client can talk to a Newer Broker
* A new Client can talk to an Older Broker
* ALWAYS USE THE LATEST CLIENT


## Projects ##
In `kafka-beginners-crouse`, you'll find a working basic Java Application demonstrating Consumer and Producer APIs.
