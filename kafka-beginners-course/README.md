## Directories
`kafka-beginners-course/kafka-basics`
Contains code for Demonstrating basic Kafka Producer and Consumer.

`kafka-beginners-course/kafka-producer-twitter`
Contains for code using Twitter API and putting the streams into Kafka.

`kafka-beginners-course/kafka-consumer-elasticsearch`
Contains code for grabbing data from broker and sending it to ElasticSearch

## Setting up Local Docker ElasticSearch
See https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
1. `docker pull docker.elastic.co/elasticsearch/elasticsearch:7.11.0`
2. ```shell
   docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 \
      -e "discovery.type=single-node" \
      -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" \
      -e "bootstrap.memory_lock=true" \
      docker.elastic.co/elasticsearch/elasticsearch:7.11.0 
   ```

**Creating Index**: `curl -X PUT localhost:9200/twitter/{type} -d {json}`

**Getting docs back:** `curl http://localhost:9200/twitter/{type}/{id}`