package org.learning.kafka.tutorial3;

import com.google.gson.JsonParser;
import org.apache.http.HttpHost;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * Consumes twitter_topic and produces an elastic search document for each record
 */
public class ElasticSearchConsumer {

    private static Logger logger = LoggerFactory.getLogger(ElasticSearchConsumer.class);
    private static JsonParser jsonParser = new JsonParser();

    public static void main(String args[]) throws IOException, InterruptedException {
        ElasticSearchConsumer elasticSearchConsumer = new ElasticSearchConsumer();
        elasticSearchConsumer.run();
    }

    public void run() throws InterruptedException, IOException {
        ElasticSearchRestClientService elasticSearchRestClientService = new ElasticSearchRestClientService("http://localhost:9200");
        logger.info("Testing...{}", elasticSearchRestClientService.getHomepage());
        RestHighLevelClient restHighLevelClientService =
                RestHighLevelClientService.getClient("localhost", 9200, "http");

        KafkaConsumer<String, String> consumer = KafkaConsumerFactory.getBootStrapConsumer("kafka-demo-elasticsearch",
                "twitter_topic");

        // Poll for new data
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

            logger.info("Received {} records", records.count());
            BulkRequest bulkRequest = new BulkRequest();

            // Inspect the records
            for (ConsumerRecord<String, String> record : records) {
                logger.info("Key: {}, Value: {}", record.key(), record.value());
                logger.info("Partition: {}, Offset: {}", record.partition(), record.offset());
                // 2 Strategies to deal with idempotent delivery. This is the case where the same tweet is sent to
                // ElasticSearch twice.
                // 1) Kafka generic ID
                //String id = record.topic() + "_" + record.partition() + "_" + record.offset();
                // 2) twitter feed specific id
                String id = extractIdFromTweet(record.value());

                // Create the entries to Elasticsearch
                IndexRequest indexRequest = new IndexRequest(
                        "twitter",
                        "tweets",
                        id // this is to make our consumer idempotent
                ).source(record.value(), XContentType.JSON);
                bulkRequest.add(indexRequest); // we add to our bulk request (takes no time)
            }

            if(records.count() > 0) {
                BulkResponse bulkItemResponses = restHighLevelClientService.bulk(bulkRequest, RequestOptions.DEFAULT);
                logger.info("Committing offsets...");
                consumer.commitAsync();
                logger.info("Offsets have been committed");
                Thread.sleep(1000);
            }
        }
        // closes client gracefully
//        restHighLevelClientService.close();
    }

    private String extractIdFromTweet(String tweetJson) {
        return jsonParser.parseString(tweetJson)
                .getAsJsonObject()
                .get("id_str")
                .getAsString();
    }

    public static class ElasticSearchRestClientService {
        private final String hostname;
        private final RestTemplate restTemplate;

        public ElasticSearchRestClientService(final String hostname) {
            this.hostname = hostname;
            this.restTemplate = new RestTemplate();
        }

        public ResponseEntity<String> getHomepage() {
            return restTemplate.getForEntity(this.hostname + "/", String.class);
        }
    }

    public static class RestHighLevelClientService {
        public static RestHighLevelClient getClient(String hostname, int port, String scheme) {
            return new RestHighLevelClient(RestClient.builder(new HttpHost(hostname, port, scheme)));
        }
    }

    public static class KafkaConsumerFactory {
        public static KafkaConsumer<String, String> getBootStrapConsumer(String groupId, String topic) {
            Properties properties = new Properties();
            // Create consumer configs
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            // Configs below is to test manual commit of offsets
            properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false"); // disable auto commit of offsets
            properties.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10");

            // Create consumer
            KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
            kafkaConsumer.subscribe(Arrays.asList(topic));
            return kafkaConsumer;
        }
    }

}
