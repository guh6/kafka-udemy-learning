package org.learning.kafka.tutorial2;


import com.twitter.hbc.core.Client;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TwitterProducer {
    static final Logger logger = LoggerFactory.getLogger(TwitterProducer.class);

    public static void main(String[] args) {
        TwitterProducer twitterProducer = new TwitterProducer();
        twitterProducer.run();
    }

    public void run(){
        // 1. create a twitter client
        // Set these as environment variables!
        String consumerKey = System.getenv("LEARNING_CONSUMER_KEY");
        String consumerSecret = System.getenv("LEARNING_CONSUMER_SECRET");
        String token = System.getenv("LEARNING_TOKEN");
        String secret = System.getenv("LEARNING_SECRET");

        TwitterClient twitterClient = new TwitterClient(consumerKey, consumerSecret, token, secret);
        // Messages gets added to the queue
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<>(100);
        Client client = twitterClient.getClient(msgQueue, "kafka", "udemy", "bitcoin");
        client.connect();

        // 2. create a kafka producer
        KafkaProducer<String, String> twitterKafkaProducer = KafkaProducerFactory.getBootstrapKafkaSafeProducer();

        // add shutdown hook - import for data availability!
        Runtime.getRuntime().addShutdownHook(new Thread(()->{
            logger.info("stopping application...");
            logger.info("shutting down client from twitter...");
            client.stop();
            logger.info("closing producer...");
            twitterKafkaProducer.close(); // forces Kafka to send the messages it has in memory
            logger.info("done!");
        }));

        // 3. loop to send tweets to kafka
        while (!client.isDone()) {
            String msg = null;
            try {
                msg = msgQueue.poll(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                client.stop();
            }
            if (msg != null) {
                logger.info(msg);
                twitterKafkaProducer.send(new ProducerRecord<>("twitter_topic", null, msg), new Callback() {
                    @Override
                    public void onCompletion(RecordMetadata metadata, Exception exception) {
                        if (exception != null) {
                            logger.error("Something bad happened", exception);
                        }
                    }
                });
            }
        }
        logger.info("End of app");
    }

}
