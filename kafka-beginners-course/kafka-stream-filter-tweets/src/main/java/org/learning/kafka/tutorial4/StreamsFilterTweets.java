package org.learning.kafka.tutorial4;

import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.math.BigInteger;
import java.util.Properties;

public class StreamsFilterTweets {

    private static BigInteger TEN_THOUSAND = new BigInteger("10000");

    public static void main(String[] args){
        // create properties
        Properties properties = new Properties();
        properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "demo-kafka-streams");
        properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        // create topology
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        // input topic - then filter it
        KStream<String, String> inputTopic = streamsBuilder.stream("twitter_topics");
        KStream<String, String> filteredStream = inputTopic.filter((k, jsonTweet) ->
                // filter for tweets which has a user of over 10000 followers
                extractUserFollowerFromTweet(jsonTweet).compareTo(TEN_THOUSAND) > 0
        );
        filteredStream.to("filtered_tweets");

        // build topology
        KafkaStreams kafkaStreams = new KafkaStreams(
                streamsBuilder.build(), properties);

        // start our stream application
        kafkaStreams.start();
    }

    private static BigInteger extractUserFollowerFromTweet(String tweetJson) {
        try {
            return JsonParser.parseString(tweetJson)
                    .getAsJsonObject()
                    .get("user")
                    .getAsJsonObject()
                    .get("followers_count")
                    .getAsBigInteger();
        } catch (Exception e) {
            return BigInteger.ZERO;
        }
    }
}
