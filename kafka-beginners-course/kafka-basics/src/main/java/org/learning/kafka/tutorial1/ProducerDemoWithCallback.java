package org.learning.kafka.tutorial1;

import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import java.util.Properties;

public class ProducerDemoWithCallback {
    static Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        // Helps Kafka on how to serialize the messages
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Creates the Producer key=string, value=string
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        // Send data - asynchronous
        for(int i =0; i < 10; i++) {
            // Create a producer record
            ProducerRecord<String, String> record = new ProducerRecord<>("second_topic", "hello world " + i);
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    // executes everytime a record is successfully sent or an exception is thrown
                    if (exception == null) {
                        logger.info("Received new metadata --" +
                                        "\nTopic: {}" +
                                        "\nPartition: {}" +
                                        "\nOffset: {}" +
                                        "\nTimestamp: {}",
                                metadata.topic(),
                                metadata.partition(),
                                metadata.offset(),
                                metadata.timestamp());
                    } else {
                        logger.error("Error while producing", exception);
                    }
                }
            });
        }
        // Flush
        producer.flush();
        // Flush and Close producer
        producer.close();
    }
}
