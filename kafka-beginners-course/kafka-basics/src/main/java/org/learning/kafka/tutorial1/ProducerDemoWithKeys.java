package org.learning.kafka.tutorial1;

import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * This demonstrates the use of key where the message with the same key will always go to the same partition
 */
public class ProducerDemoWithKeys {
    static Logger logger = LoggerFactory.getLogger(ProducerDemoWithKeys.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        // Helps Kafka on how to serialize the messages
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Creates the Producer key=string, value=string
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        // Send data - synchronous
        for(int i =0; i < 10; i++) {
            String topic = "second_topic";
            String value = "hello world " + i;
            String key = "Key " + i;

            // Create a producer record
            ProducerRecord<String, String> record = new ProducerRecord<>(topic, key, value);
            logger.info("Key={}",key);
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata metadata, Exception exception) {
                    // executes everytime a record is successfully sent or an exception is thrown
                    if (exception == null) {
                        logger.info("Received new metadata --" +
                                        "\nTopic: {}" +
                                        "\nPartition: {}" +
                                        "\nOffset: {}" +
                                        "\nTimestamp: {}",
                                metadata.topic(),
                                metadata.partition(),
                                metadata.offset(),
                                metadata.timestamp());
                    } else {
                        logger.error("Error while producing", exception);
                    }
                }
            }).get(); // block the .send() to make it synchronous - don't do this in production!
        }
        // Flush
        producer.flush();
        // Flush and Close producer
        producer.close();
    }
}
