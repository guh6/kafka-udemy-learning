package org.learning.kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * This demonstrates the proper use of threads with Kafka Consumer.
 * - Creates a thread
 * - Creates a shutdown hook
 * - Wake up the Kafka consumer when thread.shutdown() is called to properly interrupt the thread.run()
 *   operations and close down the consumer
 */
public class ConsumerDemoWithThread {
    static Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class);

    public ConsumerDemoWithThread() {}

    public static void main(String[] args) {
        ConsumerDemoWithThread consumerThread = new ConsumerDemoWithThread();
        consumerThread.run();
    }

    public void run(){
        Properties properties = new Properties();
        // To read from the beginning you can change the groupId (reset)
        String groupId = "my-sixth-application";
        String topic = "second_topic";

        // Create consumer configs
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        CountDownLatch latch = new CountDownLatch(1);
        ConsumerThread myConsumerRunnable = new ConsumerThread(latch, topic, properties);

        Thread myThread = new Thread(myConsumerRunnable);
        // tart the thread
        myThread.start();

        // Add a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(()->{
           logger.info("Caught shutdown hook");
           myConsumerRunnable.shutdown();
           try {
               latch.await();
           } catch(InterruptedException e) {
               logger.error("Application shutdown await", e);
           }
           logger.info("Applicatino has exited");
        }));

        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.error("Application got interrupted", e);
        } finally {
            logger.info("Application is closing");
        }
    }


    private static class ConsumerThread implements Runnable {

        private Logger logger = LoggerFactory.getLogger(ConsumerThread.class);

        // Allows the application to shutdown correctly
        private CountDownLatch latch;
        private final String topic;
        private KafkaConsumer<String, String> consumer;

        public ConsumerThread(CountDownLatch latch, String topic, Properties properties) {
            this.latch = latch;
            this.topic = topic;
            // Create consumer
            this.consumer = new KafkaConsumer<>(properties);
            this.consumer.subscribe(Arrays.asList(topic));

        }

        @Override
        public void run() {
            try {
                // Poll for new data
                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                    // Inspect the records
                    for (ConsumerRecord<String, String> record : records) {
                        logger.info("Key: {}, Value: {}", record.key(), record.value());
                        logger.info("Partition: {}, Offset: {}", record.partition(), record.offset());
                    }
                }
            } catch (WakeupException e) {
                logger.info("Received shutdown signal");
            } finally {
                consumer.close();
                // Tell our main code that we're done with the consumer
                latch.countDown();
            }
        }

        public void shutdown(){
            consumer.wakeup();
        }
    }

}
